const API_URL = 'https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c'
const IMG_PATH = 'https://image.tmdb.org/t/p/w1280'
const SEARCH_API = 'https://api.themoviedb.org/3/search/movie?api_key=3fd2be6f0c70a2a598f084ddfb75487c&query="'
const BASE_URL = 'https://api.themoviedb.org/3'
const API_KEY = 'api_key=3fd2be6f0c70a2a598f084ddfb75487c'

//genres list
const GENRE_LIST = 'https://api.themoviedb.org/3/genre/movie/list?api_key=3fd2be6f0c70a2a598f084ddfb75487c&language=en-US'

const main = document.getElementById('main')
const form = document.getElementById('form')
const search = document.getElementById('search')

//overview
const overview = document.getElementById('overview')

//genre related
const select_genre = document.getElementById('select_genre');

//pagination
const prev = document.getElementById('prev')
const current = document.getElementById('current')
const next = document.getElementById('next')

var currentPage = 1; 
var nextPage = 2;
var prevPage = 3;

var lastUrl = '';
var totalPages = 100;
let genreID;

// Get initial movies
getMovies(API_URL)

//get genres
getMoviesGenre(GENRE_LIST)

async function getMovies(url) {
  lastUrl = url;
  
  const res = await fetch(url)
  const data = await res.json()

  showMovies(data.results)
  currentPage = data.page;
  current.innerText = currentPage
  
  nextPage = currentPage + 1;
  prevPage = currentPage - 1;
  totalPages = data.total_pages;
}

async function getMoviesGenre(url) {
    const res = await fetch(url)
    const data = await res.json()

    showMoviesGenre(data.genres)
}

function showMoviesGenre(genres) {

    genres.forEach((genre) => {
    const {id, name} = genre;

    const genreEl = document.createElement('option')

    genreEl.setAttribute('value', `${id}`)
    genreEl.innerHTML = `${name}`

    select_genre.appendChild(genreEl);
   })
}
//end of genre

function showMovies(movies) {
    main.innerHTML = ''
    if (typeof search_term == "undefined") {
        movies.forEach((movie) => {
            const { id, title, poster_path, vote_average, overview, genre_ids} = movie

            const movieEl = document.createElement('div')
            
            movieEl.classList.add('movie')
    
                    movieEl.innerHTML = `
                    <button id="${id}" class="trailer btn btn-primary" style="width: 100%;">Trailer</button>
                    <div class="movie-image"><img src="${IMG_PATH + poster_path}" alt="${title}" id="img"/></div>
                    <div class="movie-info">
                      <button type="button" class="btn text-light" data-bs-toggle="modal" data-bs-target="#exampleModal_${id}">
                        ${title}
                      </button>
                      <span class="${getClassByRate(vote_average)}">${vote_average}</span>
                      
                      <div class="modal fade" id="exampleModal_${id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title text-dark" id="exampleModalLabel">${title}</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-dark" id="bd">
                              ${overview}
                              <hr>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  `
            main.appendChild(movieEl)
            document.getElementById(id).addEventListener('click', () => {
                console.log(id)
                openNav(movie)
            })
        
        })
    } else {
        movies.forEach((movie) => {
            const { id, title, poster_path, vote_average, overview, genre_ids} = movie

            genre_ids.forEach(gen => {
                if(genreID == gen) {

                    const movieEl = document.createElement('div')
                    movieEl.classList.add('movie')

                    movieEl.innerHTML = `
                    <button id="${id}" class="trailer btn btn-primary" style="width: 100%;">Trailer</button>
                    <div class="movie-image"><img src="${IMG_PATH + poster_path}" alt="${title}" id="img"/></div>
                    <div class="movie-info">
                      <button type="button" class="btn text-light" data-bs-toggle="modal" data-bs-target="#exampleModal_${id}">
                        ${title}
                      </button>
                      <span class="${getClassByRate(vote_average)}">${vote_average}</span>
                      
                      <div class="modal fade" id="exampleModal_${id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title text-dark" id="exampleModalLabel">${title}</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body text-dark" id="bd">
                              ${overview}
                              <hr>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  `
                  main.appendChild(movieEl)
                  document.getElementById(id).addEventListener('click', () => {
                      console.log(id)
                      openNav(movie)
                  })
                    
                }
            })

    
                  
         
        
        })
    }
    
}

//event for overlay video
const overlayContent = document.getElementById('overlay-content')
function openNav(movie) {
    let id = movie.id
    fetch(BASE_URL + '/movie/' + id + '/videos?' + API_KEY)
        .then(res => res.json())
        .then(videoData => {
           
                document.getElementById("myNav").style.width = "100%"
                
                    var embed = [];
                    videoData.results.forEach(video => {
                        let {name, key, site} = video
                        if(site == "YouTube") {
                            embed.push(`
                            <iframe width="949" height="534" src="https://www.youtube.com/embed/${key}" 
                            title="${name}" frameborder="0" 
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; 
                            gyroscope; picture-in-picture" 
                            allowfullscreen></iframe>
                            `)
                        }
                    })
                    overlayContent.innerHTML = embed.join('')
        })
    document.getElementById("myNav").style.width = "100%";
  }
  
  /* Close when someone clicks on the "x" symbol inside the overlay */
  function closeNav() {
    document.getElementById("myNav").style.width = "0%";
  }
async function getMoviesTrailer(videos) {
    const res = await fetch(videos)
    const data = await res.json()

    const video_key = data.results[0].key
    showMovies(video_key)
    console.log(data.results[0].key);
}

function getClassByRate(vote) {
    if(vote >= 8) {
        return 'green'
    } else if(vote >= 5) {
        return 'orange'
    } else {
        return 'red'
    }
}

form.addEventListener('submit', (e) => {

    e.preventDefault()

    search_term = search.value
    genreID = select_genre.value;

    if(search_term && search_term !== '') {
        getMovies(SEARCH_API + search_term)
        console.log(search_term)
        search.value = ''
    } else {
        window.location.reload()
    }
})

//event listener for pagination
prev.addEventListener('click', () => {
    if(prevPage > 0) {
        pageCall(prevPage);
    }
})

next.addEventListener('click', () => {
    if(nextPage <= totalPages) {
        pageCall(nextPage);
    }
})

function pageCall(page) {
    console.log(page)
    let url = lastUrl + '&page=' +  page;
    getMovies(url);
}
